require_relative 'receipt_item'
require_relative 'helpers'

parsed_lines = ARGF.map do |line|
  extract_info(line)
end

receipt_lines = assemble_receipt(parsed_lines)

receipt_lines.each do |receipt_line|
  puts(receipt_line)
end

