# Used a single file for several helpers to save time

# Probably should be an enum to scale a bit better
CATEGORY_EXEMPTION = {
  other: false,
  book: true,
  medical: true,
  food: true
}.freeze

ENTRY_DATA_REGEX = /(?<quantity>[0-9]+) (?<description>(?<imported>imported? )?.+) at (?<price>[0-9]+\.[0-9]{2})/

def to_multiple_of_5(value)
  (value / 5.0).ceil(0) * 5
end

# Values are managed as if they are integers. e.g., 1.50 is stored as 150.
# This file formats the before showing on screen
def stringify_currency(value)
  "%.2f" % (value / 100.0)
end

# Probably should be a service on its own, instead (related to CATEGORY_EXEMPTION above)
def category_from_description(description)
  if ['pill','medicine'].any? { |word| description.include?(word) }
    return :medical
  elsif ['book'].any? { |word| description.include?(word) }
    return :book
  elsif ['chocolate', 'tomato'].any? { |word| description.include?(word) }
    return :food
  end

  :other
end

def extract_entry_data(entry)
  matches = entry.match(ENTRY_DATA_REGEX)
  description = matches[:description]

  {
    category: category_from_description(description),
    quantity: matches[:quantity].to_i,
    price: matches[:price].to_f * 100,
    description: description,
    imported: !matches[:imported].nil?
  }
end

def assemble_receipt(items_receipt)
  total = 0
  sales_taxes = 0

  output = items_receipt.map do |item_receipt|
    sales_taxes += item_receipt.total_taxes
    total += item_receipt.total_price

    item_receipt.serialize_for_receipt
  end

  output << "Sales Taxes: #{stringify_currency(sales_taxes)}"
  output << "Total: #{stringify_currency(total)}"

  output
end

def extract_info(line)
  entry_data = extract_entry_data(line)

  ReceiptItem.new(**entry_data)
end
