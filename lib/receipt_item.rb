require_relative 'helpers'

class ReceiptItem
  def initialize(quantity:, description:, price:, imported: false, category:)
    @category = category
    @quantity = quantity
    @description = description
    @price = price
    @imported = imported
  end

  # Could be an external helper or service
  def appliable_tax_rate
    tax_rate = 0
    tax_rate += 10 if CATEGORY_EXEMPTION[@category] == false
    tax_rate += 5 if @imported == true

    tax_rate / 100.0
  end

  def item_tax
    @item_tax ||= to_multiple_of_5(@price * appliable_tax_rate)
  end

  def serialize_for_receipt
    "#{@quantity} #{@description}: #{stringify_currency(total_price)}"
  end

  def price_with_tax
    @price_with_tax ||= item_tax + @price
  end

  def total_taxes
    @total_taxes ||= item_tax * @quantity
  end

  def total_price
    @total_price ||= price_with_tax * @quantity
  end
end
