# Receipt Generator

This ruby application generate a purchase receipt, including taxes, based on a product list provided to it.

Some choices were made deliberately considering challenge scope. Most of the code should be covered by test.

Code style is a bit inconsistent, as no linter was employed on the project.

## Setup

1. Please ensure your environment executes `ruby 3.0.3`. It can be installed through `rbenv`:
```
rbenv install
```

2. Install project dependencies
```
gem install bundler && bundle install
```

## Usage Instructions

Provide file `lib/main.rb` as input for ruby. Then, it will request the desired product list, with one product entry per line.
It support Linux Standard Stream (STDIN), meaning files can also be provided as input.

There are three files containing input examples, based on data provided with the challenge.
For instance, the following commands will use them as input:

### Input 1
```
ruby lib/main.rb < spec/fixtures/input1.txt
```

### Input 2
```
ruby lib/main.rb < spec/fixtures/input2.txt
```

### Input 3
```
ruby lib/main.rb < spec/fixtures/input3.txt
```

## Testing

Rspec is included in this project. To execute the test suite, which also includes the above test cases, execute the following:
```
rspec --format doc
```


