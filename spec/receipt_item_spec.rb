require_relative '../lib/receipt_item'

def init_receipt(**args)
  ReceiptItem.new(quantity: 5, description: 'Foo', price: 100, imported: false, category: :other, **args)
end

describe "#appliable_tax_rate" do
  it "calculates 10% tax rate for non-exempt local goods" do
    receipt = init_receipt

    expect(receipt.appliable_tax_rate).to eq(0.1)
  end

  it "calculates 0% tax rate for exempt local goods" do
    receipt = init_receipt(category: :food)

    expect(receipt.appliable_tax_rate).to eq(0)
  end

  it "calculates 5% tax rate for exempt imported goods" do
    receipt = init_receipt(category: :food, description: 'imported Foo', imported: true)

    expect(receipt.appliable_tax_rate).to eq(0.05)
  end

  it "calculates 15% tax rate for non-exempt imported goods" do
    receipt = init_receipt(description: 'imported Foo', imported: true)

    expect(receipt.appliable_tax_rate).to eq(0.15)
  end
end

describe "#item_tax" do
  it "stores cents using whole numbers" do
    receipt = init_receipt

    expect(receipt.item_tax).to eq(10)
  end
end

describe "#total_price" do
  it "stores cents using whole numbers" do
    receipt = init_receipt

    expect(receipt.total_price).to eq(550)
  end
end

describe "#total_taxes" do
  it "stores cents using whole numbers" do
    receipt = init_receipt

    expect(receipt.total_taxes).to eq(50)
  end
end

describe "#serialize_for_receipt" do
  it "serializes item entry for receipt" do
    receipt = init_receipt

    expect(receipt.serialize_for_receipt).to eq('5 Foo: 5.50')
  end
end
