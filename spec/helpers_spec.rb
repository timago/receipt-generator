require_relative '../lib/helpers'

describe "#to_multiple_of_5" do
  it "rounds up value to a multiple of 5" do
    receipt = init_receipt

    expect(to_multiple_of_5(1)).to eq(5)
    expect(to_multiple_of_5(5)).to eq(5)

    expect(to_multiple_of_5(6)).to eq(10)
    expect(to_multiple_of_5(10)).to eq(10)
  end
end

describe "#stringify_currency" do
  it "formats currency values" do
    expect(stringify_currency(0)).to eq('0.00')
    expect(stringify_currency(10)).to eq('0.10')
    expect(stringify_currency(199)).to eq('1.99')
    expect(stringify_currency(5000)).to eq('50.00')
  end
end

describe "#category_from_description" do
  it "parses item category from description" do
    expect(category_from_description('packet of headache pills')).to eq(:medical)
    expect(category_from_description('bottle of perfume')).to eq(:other)
    expect(category_from_description('imported bottle of perfume')).to eq(:other)
    expect(category_from_description('imported boxes of chocolates')).to eq(:food)
    expect(category_from_description('books')).to eq(:book)
  end
end

describe "#extract_entry_data" do
  it "creates hash based on item data" do
    expect(extract_entry_data('2 book at 12.49')).to eq({
      category: :book,
      quantity: 2,
      price: 1249.0,
      description: 'book',
      imported: false
    })
  end

  it "identifies imported items" do
    expect(extract_entry_data('1 imported bottle of perfume at 47.50')).to eq({
      category: :other,
      quantity: 1,
      price: 4750.0,
      description: 'imported bottle of perfume',
      imported: true
    })
  end
end

describe "#assemble_receipt" do
  it "returns an array containing receipt lines" do
    items = [
      extract_info('2 book at 12.49'),
      extract_info('1 imported bottle of perfume at 47.50')
    ]

    expected_receipt = [
      "2 book: 24.98",
      "1 imported bottle of perfume: 54.65",
      "Sales Taxes: 7.15",
      "Total: 79.63"
    ]

    expect(
      assemble_receipt(items)
    ).to(
      eq(expected_receipt)
    )
  end
end
