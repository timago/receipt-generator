require_relative '../lib/helpers'

def fixture_receipt(fixture_name)
  content = File.readlines "#{__dir__}/fixtures/#{fixture_name}"

  parsed_lines = content.map { |line| extract_info(line) }

  assemble_receipt(parsed_lines)
end

describe 'main' do
  it "returns correct output for input1.txt" do
    receipt_lines = fixture_receipt('input1.txt')

    expect(receipt_lines).to eq([
      "2 book: 24.98",
      "1 music CD: 16.49",
      "1 chocolate bar: 0.85",
      "Sales Taxes: 1.50",
      "Total: 42.32"
    ])
  end

  it "returns correct output for input2.txt" do
    receipt_lines = fixture_receipt('input2.txt')

    expect(receipt_lines).to eq([
      "1 imported box of chocolates: 10.50",
      "1 imported bottle of perfume: 54.65",
      "Sales Taxes: 7.65",
      "Total: 65.15"
    ])
  end

  it "returns correct output for input3.txt" do
    receipt_lines = fixture_receipt('input3.txt')

    expect(receipt_lines).to eq([
      "1 imported bottle of perfume: 32.19",
      "1 bottle of perfume: 20.89",
      "1 packet of headache pills: 9.75",
      "3 imported boxes of chocolates: 35.55",
      "Sales Taxes: 7.90",
      "Total: 98.38"
    ])
  end
end
